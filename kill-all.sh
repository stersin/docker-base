#!/bin/bash

DOCKER_TAG=`cat ./Dockertag`

running_containers=$(sudo docker ps | grep $DOCKER_TAG | awk -F' ' '{ print $1 }')

if [ ! "running_containers" = "" ]
then
        echo "==== Killing running containers for image $DOCKER_TAG ..."
        for launched_instance_id in $running_containers
        do
                echo "Container killed : $(sudo docker kill $launched_instance_id)"
        done

        echo "Done ===="
fi
