FROM ubuntu:trusty
MAINTAINER Sebastien TERSIN, stersin.dev@gmail.com

ENV DEBIAN_FRONTEND noninteractive

# Base upgrade
RUN apt-get update
RUN apt-get upgrade -y

# Create main user and make it sudoer
RUN apt-get install -y sudo
RUN useradd -d /home/user -m -s /bin/bash user
RUN echo user:user | chpasswd

# Add public key to authorized keys file
RUN mkdir -p /home/user/.ssh
ADD .ssh/id_rsa /home/user/.ssh/id_rsa
ADD .ssh/id_rsa.pub /home/user/.ssh/id_rsa.pub
ADD .ssh/authorized_keys /home/user/.ssh/authorized_keys
RUN chown -R user:user /home/user/.ssh
RUN chmod -R 700 /home/user/.ssh

# Make user sudoer
RUN echo 'user ALL=(ALL) NOPASSWD:ALL' >> /etc/sudoers.d/user
RUN chmod 0440 /etc/sudoers.d/user

# Install sshd
RUN apt-get install -y openssh-server
RUN mkdir -p /var/run/sshd
## patching issue https://github.com/docker/docker/issues/5663
RUN sed -ri 's/^session\s+required\s+pam_loginuid.so$/session optional pam_loginuid.so/' /etc/pam.d/sshd

# Install supervisord
RUN apt-get install -y supervisor
RUN mkdir -p /var/log/supervisor
RUN mkdir -p /etc/supervisor/conf.d
ADD etc/supervisor/conf.d/sshd.conf /etc/supervisor/conf.d/sshd.conf

EXPOSE 22

CMD ["supervisord", "-n"]
