#!/bin/bash

DOCKER_TAG=`cat ./Dockertag`

container_id=$(sudo docker ps | grep $DOCKER_TAG | awk -F\s '{ print $1 }')
ssh_url=$(sudo docker port $container_id 22)

ssh_interface=$(echo $ssh_url | awk -F  ":" '/1/ {print $1}')
ssh_port=$(echo $ssh_url | awk -F  ":" '/1/ {print $2}')

ssh user@$ssh_interface -i ./.ssh/id_rsa -o UserKnownHostsFile=/dev/null -o StrictHostKeyChecking=no -p $ssh_port 
